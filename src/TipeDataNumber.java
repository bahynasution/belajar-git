public class TipeDataNumber {
    public static void main(String[] args) {

        byte iniByte = 10;
        short iniShort = 1000;
        int iniInt = 10000;
        long iniLong = 1000000;

        float iniFloat = 1.12F;
        double iniDouble = 10.10;

        int iniDecimal = 34;
        int iniBinary = 0b11;
        int iniHexadecimal = 0x1df23;

        //konversi tipe data byte -> double
        byte iniByte1 = 100;
        short iniShort1 = iniByte1;
        int iniInt1 = iniShort1;
        long iniLong1 = iniInt1;
        float iniFloat1 = iniLong1;
        double iniDouble1 = iniFloat1;

        //konversi tipe data double -> byte
        double iniDouble2 = 10000000;
        float iniFloat2 = (float) iniDouble2;
        long iniLong2 = (long) iniFloat2;
        int iniInt2 = (int) iniLong2;
        short iniShort2 = (short)  iniInt2;
        byte iniByte2 = (byte) iniShort2;








    }
}
